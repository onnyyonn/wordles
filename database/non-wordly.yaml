- name: Waffle
  url: https://wafflegame.net/
  emoji: 🧇
  object: Drag and rearrange letters into words in a 5×5 [magic square](https://en.wikipedia.org/wiki/Magic_square) with four holes (hence ‘Waffle’)
  feedback: Wordle colour coding
  developer:
    - name: James Robinson
      url: https://twitter.com/jamesjessian
  announcement:
    date: 2022-02-13
    url: https://twitter.com/JamesJessian/status/1492942028799451138

- name: alphabattle.xyz
  url: https://alphabattle.xyz/
  object: Words; supports different languages and length
  feedback: '[Bulls and Cows](https://en.wikipedia.org/wiki/Bulls_and_Cows) rules'
  note: Play alone or with a friend; sleek B/W design; preceded Wordle
  emoji: ⇶
  announcement:
    date: 2020-04-09
    url: https://twitter.com/alphabattle_xyz/status/1248298051275501570

- name: Guess my word
  url: https://hryanjones.com/guess-my-word/
  src: https://github.com/hryanjones/guess-my-word
  object: English words (any length)
  feedback: Alphabetic order
  note: Preceded Wordle
  emoji: 🔖
  developer:
    - name: H. Ryan Jones
      url: https://github.com/hryanjones

- name: Semantle
  lang: eng
  url: https://semantle.novalis.org/
  object: English words (any length)
  feedback: 'Semantic distance using [word2vec](https://en.wikipedia.org/wiki/Word2vec)’s metrics'
  emoji: 🕸
  developer:
    - name: David Turner
      url: https://novalis.org/
  announcement:
    date: 2022-02-01
    url: https://twitter.com/NovalisDMT/status/1488356261875830784

- name: סמנטעל
  lang: heb
  css: hebrew
  url: https://semantle-he.herokuapp.com/
  src: https://github.com/ishefi/semantle-he
  object: Hebrew words (any length)
  feedback: 'Semantic distance using [word2vec](https://reshetech.co.il/machine-learning-tutorials/gensim-and-word2vec)’s metrics'
  emoji: 🕸
  developer:
    - name: Itamar Shefi
      url: https://github.com/ishefi
  announcement:
    date: 2022-02-21
    url: https://twitter.com/shefi89/status/1495687324964990978
  reference: https://twitter.com/shefi89/status/1495699836468973568
  media:
    - url: https://www.globes.co.il/news/article.aspx?did=1001403566

- name: Swemantle
  lang: swe
  url: https://swemantle.riddle.nu/
  src: https://github.com/BionicRiddle/swemantle
  object: Swedish words (any length)
  feedback: 'Semantic distance using [word2vec](https://www.ida.liu.se/divisions/hcs/nlplab/swectors/)’s metrics'
  emoji: 🕸
  developer:
    - name: BionicRiddle
      url: https://github.com/BionicRiddle

- name: Semantle en español
  lang: spa
  url: http://semantle-es.cgk.cl/
  object: Spanish words (any length)
  feedback: 'Semantic distance using [word2vec](https://crscardellino.ar/SBWCE/)’s metrics'
  emoji: 🕸

- name: Three Magic Words
  url: https://www.threemagicwords.app/
  object: Three five-letter English words
  feedback: 'You drag letters from an inventory to see if they fit'
  note: 'Some letters are given'
  emoji: 🪄
  reference: https://news.ycombinator.com/item?id=30262341

- name: Stackle
  lang: eng
  url: https://www.stackle.fun/
  object: Add words by changing one letter and rearranging the letters of the previous ones
  feedback: Whether the word is in the wordlist and follows the rules
  emoji: 📚
  announcement:
    date: 2022-01-27
    url: https://twitter.com/StackleWordGame/status/1486720693811113987

- name: Phrasle
  url: https://phrasle.com/
  object: English phrases
  feedback: '[Wheel of Fortune](https://en.wikipedia.org/wiki/Wheel_of_Fortune_(American_game_show)) rules'
  emoji: 🎡
  developer:
    - name: Rommel Santor
      url: http://rommelsantor.com/
  reference: https://www.microsiervos.com/archivo/puzzles-y-rubik/variantes-wordle.html

- name: Numbro
  url: https://kveez.com/en/numbro/
  emoji: 🔢
  object: 'Four-digit numbers'
  feedback: '[Bulls and Cows](https://en.wikipedia.org/wiki/Bulls_and_Cows) rules'

- name: instant Nerdle
  url: https://instant.nerdlegame.com/
  emoji: 🧮
  object: Rearrange mathematical symbols so they make a valid equation
  feedback: 'A set of symbols is given; one attempt; positive feedback if the result is a valid equation'
  social:
    - network: Twitter
      url: https://twitter.com/nerdlegame
  reference: https://gitlab.com/rwmpelstilzchen/wordles/-/issues/16

- name: Funcdle
  url: https://hjaem.info/funcdle.html
  src: https://github.com/Medowhill/funcdle
  emoji: 📈
  object: Mathematical formulas (*f(x)*)
  feedback: 'If a point on the guess *y=f(x)* graph is vertically different from the target by less than 0.5, it is colored black; otherwise — grey. In addition, Wordle rules are used for the mathematical symbols.'
  developer:
    - name: Jaemin Hong
      url: https://hjaem.info/

- name: Worldle
  url: https://worldle.teuteuf.fr/
  emoji: 🗺
  object: 'A silhouette of a country or a territory is given; the goal is to identify it'
  feedback: Direction and distance
  announcement:
    date: 2022-01-24
    url: https://twitter.com/teuteuf/status/1485628624929705988

- name: Worldle
  url: https://yawgmoth.github.io/worldle/
  emoji: 🗺
  object: 'A silhouette and the flag of a country or a territory is given; the goal is to identify it'
  feedback: Distance and trivia
  developer:
    - name: Markus Eger
      url: https://github.com/yawgmoth
  announcement:
    date: 2022-01-19
    url: https://twitter.com/Yawgmoth46/status/1483713741178871811
  reference: https://twitter.com/Yawgmoth46/status/1493867476022489088

- name: Globle
  url: https://globle-game.com/
  src: https://github.com/the-abe-train/globle
  emoji: 🌏
  object: Countries and territories
  feedback: Distance
  developer:
    - name: Abe Train
      url: https://the-abe-train.com/
  announcement:
    date: 2022-01-29
    url: https://twitter.com/theAbeTrain/status/1487443791548194816
  media:
    - url: https://www.forbes.com/sites/erikkain/2022/02/07/learn-geography-with-this-wordle-spinoff-globle/
    - url: https://kotaku.com/globle-wordle-games-like-tree-puzzle-geography-game-bro-1848492190
  reference: https://gitlab.com/rwmpelstilzchen/wordles/-/issues/3

- name: estadi.ooo
  url: http://estadi.ooo/
  emoji: ⚽
  object: 'A picture of a soccer stadium in Brazil is given; the goal is to identify it'
  feedback: 'Trivia about the stadium'
  note: 'Portuguese interface; 5 guesses'
  developer:
    - name: Rodrigo Menegat
      url: https://twitter.com/RodrigoMenegat
  announcement:
    date: 2022-02-14
    url: https://twitter.com/RodrigoMenegat/status/1493286627980058630

- name: Birdle
  url: https://www.birdlegame.com/
  emoji: 🦆
  object: UK bird
  feedback: 'A picture of the bird is given; [hangman](https://en.wikipedia.org/wiki/Hangman_(game)) rules'

- name: Cluedle
  url: https://celadon.ai/cluedle
  emoji: ⭐️
  object: Four emoji
  feedback: Choose one emoji and see whether it is in the target; clues
  developer:
    - name: Celadon
      url: https://celadon.ai/
  announcement:
    date: 2022-02-10
    url: https://twitter.com/SoophieSilver/status/1491637938232623107

- name: Flaggle
  url: https://ducc.pythonanywhere.com/flaggle/
  src: https://github.com/theducvu/flaggle
  emoji: 🏁
  object: The flag of a country or territory
  feedback: 'Similarity as a [mask](https://en.wikipedia.org/wiki/Mask_(computing)#Image_masks) marking the pixels that share colour with the target'
  developer:
    - name: Duc Vu
      url: https://twitter.com/gastricsparrow
  announcement:
    date: 2022-02-16
    url: https://twitter.com/GastricSparrow/status/1493766980209299468

- name: Shaple
  url: https://swag.github.io/shaple/
  emoji: 🔶
  object: Match rotating cubes
  feedback: Correct / incorrect
  note: Related to Wordle only in name
  developer:
    - name: Ravi Parikh
      url: https://twitter.com/ravisparikh

- name: Linkr
  url: https://www.playlinkr.net/
  emoji: 🕸
  object: 'Connect pairs of vertices on a [graph](https://en.wikipedia.org/wiki/Graph_(discrete_mathematics))'
  feedback: Indication if succeeded; ‘helper mode’ marks correct paths
  note: Related to Wordle only in name and the fact it has daily puzzles
  developer:
    - name: Charles Matthews
  announcement:
    date: 2022-02-16
    url: https://twitter.com/LINKR__/status/1494019221839355904
  reference: https://twitter.com/waxpancake/status/1491491143019757568

- name: Squirdle
  lang: eng
  url: https://squirdle.fireblend.com/
  src: https://github.com/Fireblend/squirdle
  object: Pokémon names
  feedback: '[Order relation](https://en.wikipedia.org/wiki/Order_(mathematics)) for the generation, height and weight of the Pokémon; [set membership](https://en.wikipedia.org/wiki/Element_(mathematics)#Notation_and_terminology) for types'
  note: Daily and unlimited versions
  emoji: 🧢
  developer:
    - name: Sergio Morales E.
      url: http://www.fireblend.com/
  announcement:
    date: 2022-01-11
    url: https://twitter.com/fireblend/status/1480770147833024520
  media:
    - url: https://www.theguardian.com/games/2022/feb/06/worried-about-losing-wordle-here-are-some-alternatives-just-in-case

- name: Squirdle
  lang: eng
  url: https://squirdle.polv.cc/
  src: https://github.com/patarapolw/squirdle
  object: Pokémon names
  feedback: '[Order relation](https://en.wikipedia.org/wiki/Order_(mathematics)) for the generation, height and weight of the Pokémon; [set membership](https://en.wikipedia.org/wiki/Element_(mathematics)#Notation_and_terminology) for types'
  note: >
    [Japanese](https://ja-squirdle.vercel.app/) and [Korean](https://ko-squirdle.vercel.app/);
    daily and unlimited versions
  emoji: 🧢
  developer:
    - name: Pacharapol Withayasakpunt
      url: https://www.polv.cc/

- name: WorDOL
  url: https://app.39m.ltd/games/wordol/
  emoji: 🦋
  object: '[*The Idolmaster Million Live!*](https://en.wikipedia.org/wiki/The_Idolmaster_Million_Live!) (アイドルマスター ミリオンライブ！) characters'
  feedback: Green indicates whether a selected character is of the same unit and type of the target character
  announcement:
    date: 2022-02-12
    url: https://twitter.com/mltd_app/status/1492347959631310851

- name: Commander Codex
  url: https://commandercodex.com/
  emoji: 🧙
  object: 'Commander cards from [*Magic: The Gathering*](https://en.wikipedia.org/wiki/Magic:_The_Gathering)'
  feedback: '[Containment](https://en.wikipedia.org/wiki/Subset) and [order](https://en.wikipedia.org/wiki/Order_(mathematics)) relations of various features'
  developer:
    - name: Archidekt
      url: https://archidekt.com/
  announcement:
    date: 2022-01-26
    url: https://twitter.com/archidekt/status/1486444983012007940

- name: Datele
  url: https://pudding.pro/games/datele/
  emoji: 📅
  object: Date
  feedback: Range for the day and month; Wordle-like rules for the digits of the date
  developer:
    - name: Ruslan Sibgatullin
      url: https://pudding.pro/
